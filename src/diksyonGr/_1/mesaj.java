package diksyonGr._1;

import diksyonGr.mo;

public class mesaj implements mo{
	// TODO
	// L'absurde naît de cette confrontation entre l'appel humain et le silence déraisonnable du monde;
	@Override
	public String gettGkst(){
		absurd rezult=new absurd(new sGt(new kMfrMtasyM(new apGl(new umN()),new derezonabl(new silAs(new mMd())))));
		return rezult.gettGkst();
	}
	
	/*Dans cette phrase est concentrée la puissance d'un conflit, d'une confrontation qui sous-tend et emporte l'œuvre de Camus;
	Deux forces s'opposent : l'appel humain à connaître sa raison d'être et l'absence de réponse du milieu où il se trouve, l'homme vivant dans un monde dont il ne comprend pas le sens, dont il ignore tout, jusqu'à sa raison d'être;
	L'appel humain, c'est la quête d'une cohérence, or pour Camus il n'y a pas de réponse à ce questionnement sur le sens de la vie;
	Tout au moins n'y a-t-il pas de réponse satisfaisante, car la seule qui pourrait satisfaire l'écrivain devrait avoir une dimension humaine : « Je ne puis comprendre qu'en termes humains »;
	Ainsi les religions qui définissent nos origines, qui créent du sens, qui posent un cadre, n'offrent pas de réponse pour l'homme absurde : « Je ne sais pas si ce monde a un sens qui le dépasse;
	Mais je sais que je ne connais pas ce sens et qu'il m'est impossible pour le moment de le connaître;
	Que signifie pour moi une signification hors de ma condition ? »;
	L'homme absurde n'accepte pas de perspectives divines, il veut des réponses humaines;
	*/
}

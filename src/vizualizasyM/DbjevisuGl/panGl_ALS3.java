package vizualizasyM.DbjevisuGl;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
public class panGl_ALS3 extends JPanel{
	private static final long serialVersionUID=1L;
	public ArrayList<String>  ALS;
	public panGl_ALS3(ArrayList<String> ALS){
		this.ALS=ALS;
		bwild();
	}
	public void bwild(){
		removeAll();
		setLayout(new GridLayout(ALS.size(),1));
		int l=ALS.size();
		for(int i=0;i<l;i++){
			JTextArea JTA=new JTextArea(ALS.get(i));
			JTA.setFont(new Font("Cambria",Font.PLAIN,20));
			add(JTA);
		}
	}
}
package vizualizasyM.DbjevisuGl;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JTextArea;
public class tGkst_area extends JTextArea{
	public static final long serialVersionUID=1L;
	ArrayList<String>		 ALS;
	public tGkst_area(){}
	public tGkst_area(ArrayList<String> ALS){
		this.ALS=ALS;
	}
	public void paintComponent(Graphics g){
		Font font=new Font("Courier",Font.BOLD,15);
		g.setFont(font);
		g.setColor(Color.red);
		int l=ALS.size();
		for(int i=0;i<l;i++){
			g.drawString((String)ALS.get(i),10,i*15+15);
		}
	}
}

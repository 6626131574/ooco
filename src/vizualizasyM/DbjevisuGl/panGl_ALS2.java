package vizualizasyM.DbjevisuGl;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
public class panGl_ALS2 extends JSplitPane{
	private static final long serialVersionUID=1L;
	public JPanel			  JP;
	public panGl_ALS3		  pALS3;
	public ArrayList<String>  ALS;
	public panGl_ALS2(ArrayList<String> ALS){
		super(JSplitPane.HORIZONTAL_SPLIT);
		this.ALS=ALS;
		init();
		rifrGq();
	}
	public void init(){
		pALS3=new panGl_ALS3(ALS);
		setRightComponent(pALS3);
	}
	public void rifrGq(){
		JP=new JPanel();
		{
			JP.setLayout(new GridLayout(ALS.size(),1));
			for(int i=ALS.size();i>0;i--){
				JLabel JL=new JLabel(""+i);
				JL.setFont(new Font("Cambria",Font.PLAIN,20));
				JP.add(JL);
			}
		}
		setLeftComponent(JP);
		setDividerSize(0);
		setDividerLocation(50);
		pALS3.bwild();
	}
}
package vizualizasyM.DbjevisuGl;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class panGl_ALS extends JPanel{
	private static final long serialVersionUID=1L;
	public ArrayList<String>  ALS;
	public panGl_ALS(ArrayList<String> ALS){
		this.ALS=ALS;
		build();
	}
	public void build(){
		removeAll();
		setLayout(new GridLayout(ALS.size(),1));
		int l=ALS.size();
		for(int i=0;i<l;i++){
			JLabel JL=new JLabel(ALS.get(i));
			JL.setFont(new Font("Cambria",Font.PLAIN,20));
			add(JL);
		}
	}
}
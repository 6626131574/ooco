package vizualizasyM.DbjevisuGl;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class panGlALALS extends JPanel{
	public static final long			serialVersionUID=1L;
	public ArrayList<ArrayList<String>>	ALALS			=new ArrayList<>();
	public panGlALALS(ArrayList<ArrayList<String>> ALALS){
		this.ALALS=ALALS;
		bwild();
	}
	public void bwild(){
		removeAll();
		setLayout(new GridLayout(ALALS.size(),1));
		for(ArrayList<String> ALS:this.ALALS){
			JPanel JP = new JPanel();
			JP.setLayout(new GridLayout(ALS.size()+1,1));
			for(Object O:ALS){
				JP.add(new JLabel((String) O));
			}
			JP.add(new JLabel(" "));
			add(JP);
		}
	}
}

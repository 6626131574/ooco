package vizualizasyM.DbjevisuGl.fEnGtR;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import modGllojik.tools;
import modGllojik.algoritm.jeneratLR.mo_aleatwar;
import vizualizasyM.ekritur.alfabe;
public class Random_manager extends JFrame{
	private static final long	serialVersionUID=-3782979681589270590L;
	public ArrayList<String>	itemList;
	public JTextArea			itemListPrinter;
	public alfabe				alf;
	public Random_manager(){
		init();
	}
	@SuppressWarnings("static-access")
	public void init(){
		setVisible(true);
		setTitle("jGstyonGr daleatwar");
		setSize(1000,600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JSplitPane mainPanel=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		mainPanel.setResizeWeight(0.2);
		mainPanel.setDividerSize(1);
		setContentPane(mainPanel);
		try{
			alf=new alfabe(new File("bd/alfabe(tocaki).txt"));
		}catch(Exception e){
			e.printStackTrace();
			JOptionPane JOP=new JOptionPane();
			JOP.showMessageDialog(null,"File:bd/alfabe(tocaki).txt\nerLR\nFile ¬qarje→¬(alfabe)","erLR",JOptionPane.ERROR_MESSAGE);
		}
		{
			JPanel JP=new JPanel();
			JP.setLayout(new GridLayout(3,2));
			JButton JB=new JButton("qarje 1 list");
			{
				JB.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent event){
						try{
							itemList=tools.File_to_ALS(tools.import_File());
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				});
			}
			JP.add(JB);
			mainPanel.setTopComponent(JP);
		}
		{
			JSplitPane JSP2=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
			JSP2.setResizeWeight(0.2);
			JSP2.setDividerSize(5);
			itemListPrinter=new JTextArea("ryN");
			itemListPrinter.setAutoscrolls(true);
			itemListPrinter.setLineWrap(true);
			JSP2.setRightComponent(itemListPrinter);
			JButton JB=new JButton("jenGr");
			JB.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						int i=Integer.parseInt(itemListPrinter.getText());
						itemListPrinter.setText(mo_aleatwar.jenGrmo(alf,i));
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			JSP2.setLeftComponent(JB);
			mainPanel.setBottomComponent(JSP2);
		}
	}
}

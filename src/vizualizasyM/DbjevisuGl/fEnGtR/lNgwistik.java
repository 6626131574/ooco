package vizualizasyM.DbjevisuGl.fEnGtR;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import modGllojik.tools;
import modGllojik.algoritm.jeneratLR.mo_aleatwar;
import vizualizasyM.ekritur.alfabe;
public class lNgwistik extends JFrame{
	private static final long serialVersionUID=8783990311153165628L;
	public alfabe alf;
	public JTextArea JTA1,JTA2,JTA3;
	public lNgwistik(){
		init();
	}
	@SuppressWarnings("static-access")
	public void init(){
		setVisible(true);
		setTitle("moaleatwar");
		setSize(1000,600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JSplitPane JSP1=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSP1.setResizeWeight(0.2);
		JSP1.setDividerSize(1);
		setContentPane(JSP1);
		{
			JPanel JP=new JPanel();
			JP.setLayout(new GridLayout(3,2));
			JButton JB=new JButton("qarje(alfabe)");
			{
				JB.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent event){
						try{
							alf=new alfabe(tools.import_File());
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				});
			}
			JP.add(JB);
			JP.add(new JButton());
			JP.add(new JLabel("nMbr(fonGm)"));
			JP.add(new JLabel("struktur(fonGm)"));
			JTA1=new JTextArea("4");
			JP.add(JTA1);
			JTA2=new JTextArea("struktur(fonGm)");
			JP.add(JTA2);
			JSP1.setTopComponent(JP);
		}
		{
			JSplitPane JSP2=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
			JSP2.setResizeWeight(0.2);
			JSP2.setDividerSize(5);
			JTA3=new JTextArea("ryN");
			JTA3.setAutoscrolls(true);
			JTA3.setLineWrap(true);
			JSP2.setRightComponent(JTA3);
			JButton JB=new JButton("jenGr");
			JB.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						int i=Integer.parseInt(JTA1.getText());
						JTA3.setText(mo_aleatwar.jenGrmo(alf,i));
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			JSP2.setLeftComponent(JB);
			JSP1.setBottomComponent(JSP2);
		}
		try{
			alf=new alfabe(new File("bd/alfabe(tocaki).txt"));
		}catch(Exception e){
			e.printStackTrace();
			JOptionPane JOP=new JOptionPane();
			JOP.showMessageDialog(null,"File:bd/alfabe(tocaki).txt\nerLR\nFile ¬qarje→¬(alfabe)","erLR",JOptionPane.ERROR_MESSAGE);
		}
	}
}

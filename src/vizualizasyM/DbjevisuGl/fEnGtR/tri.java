package vizualizasyM.DbjevisuGl.fEnGtR;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import modGllojik.tools;
import vizualizasyM.DbjevisuGl.panGl_ALS;
import vizualizasyM.DbjevisuGl.PanelToSortText;
public class tri extends JFrame{
	public static final long	serialVersionUID=1L;
	public ArrayList<Object>	A;
	public panGl_ALS			pt;
	public JSplitPane			JSpP;
	public JScrollPane			leftPanel,rightPanel;
	public PanelToSortText		panelForText;
	public tri() throws Exception{
		init();
		init_mEnu();
	}
	private void init() throws Exception{
		A=new ArrayList<>();
		JSpP=new JSplitPane();
		leftPanel=new JScrollPane();
		rightPanel=new JScrollPane();
		setVisible(true);
		setTitle("tri");
		setSize(1000,1000);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JSpP=new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		setContentPane(JSpP);
		JSpP.setDividerSize(3);
		JSpP.setResizeWeight(0.6);
		JSpP.setContinuousLayout(true);
		JSpP.setLeftComponent(leftPanel);
		leftPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		leftPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JSpP.setRightComponent(rightPanel);
		rightPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		rightPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	}
	public void Display_PanelToSortText(panGl_ALS pt) throws Exception{
		panelForText=new PanelToSortText(pt.ALS);
		rightPanel=new JScrollPane(panelForText);
		rightPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		rightPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JSpP.setRightComponent(rightPanel);
	}
	public JMenuBar		JMB;
	public JMenu		JM1,JM2,JM3;
	public JMenuItem	JMI1,JMI2,JMI3,JMI4,JMI5,JMI6,JMI7;
	public void init_mEnu(){
		JMB=new JMenuBar();
		setJMenuBar(JMB);
		JM1=new JMenu("aksyM");
		JM2=new JMenu("Gd");
		JMB.add(JM1);
		JMB.add(JM2);
		JMI1=new JMenuItem("NpDR_File");
		JMI1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				try{
					File file=tools.import_File();
					if(file!=null){
						pt=new panGl_ALS(tools.File_to_ALS(file));
						leftPanel=new JScrollPane(pt);
						leftPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
						JSpP.setLeftComponent(leftPanel);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		JMI1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1,InputEvent.CTRL_MASK));
		JM1.add(JMI1);
		JMI4=new JMenuItem("triye_la_list");
		JMI4.addActionListener(new ActionListener(){
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent event){
				try{
					if(pt!=null){
						Display_PanelToSortText(pt);
					}else{
						JOptionPane jop1;
						jop1=new JOptionPane();
						jop1.showMessageDialog(null,"ilnyapadElistatriye","mesaj",JOptionPane.ERROR_MESSAGE);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		JMI4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD2,InputEvent.CTRL_MASK));
		JM1.add(JMI4);
		JMI7=new JMenuItem("triye_list_jenerik");
		JMI7.addActionListener(new ActionListener(){
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent event){
				try{
					JOptionPane jop=new JOptionPane();
					String S=jop.showInputDialog(null,"Atre lE nMbr ditGm a trie","kGstyM",JOptionPane.QUESTION_MESSAGE);
					if(S!=null){
						int l=10;
						try{
							l=Integer.parseInt(S);
							ArrayList<String> ALS=new ArrayList<>();
							for(int i=1;i<=l;i++){
								ALS.add(""+i);
							}
							pt=new panGl_ALS(ALS);
							Display_PanelToSortText(pt);
						}catch(Exception e){
							JOptionPane jop1;
							jop1=new JOptionPane();
							jop1.showMessageDialog(null,"ilfodone1int","mesaj",JOptionPane.ERROR_MESSAGE);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		JMI7.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD4,InputEvent.CTRL_MASK));
		JM1.add(JMI7);
		JMI2=new JMenuItem("GkspDRt_sH_fDrma(tGkst)");
		JMI2.addActionListener(new ActionListener(){
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent event){
				try{
					if(panelForText!=null){
						tools.ALS_to_file_manual(panelForText.jtf.coupleOfListForSort.get(1).get(0));
					}else{
						JOptionPane jop1;
						jop1=new JOptionPane();
						jop1.showMessageDialog(null,"ilnyapadElistaArEjistre","mesaj",JOptionPane.ERROR_MESSAGE);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		});
		JMI2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD3,InputEvent.CTRL_MASK));
		JM1.add(JMI2);
		JMI3=new JMenuItem("rifrGq_la_fEnGtR");
		JMI3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				repaint();
			}
		});
		JMI3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5,0));
		JM1.add(JMI3);
		JM3=new JMenu("Language");
		JM1.add(JM3);
		JMI5=new JMenuItem("€ooco");
		JMI5.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				// TODO
				System.out.println("il faut faire le language �ooco");
			}
		});
		JM3.add(JMI5);
		JMI6=new JMenuItem("Français");
		JMI6.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				// TODO
				System.out.println("il faut faire le language Fran�ais");
			}
		});
		JM3.add(JMI6);
	}
}

package vizualizasyM.DbjevisuGl.fEnGtR;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import modGllojik.tools;
import vizualizasyM.DbjevisuGl.panGl_ALS2;
public class DrdonAsLR extends JFrame{
	public static final long serialVersionUID=1L;
	public JSplitPane JSP;
	public ArrayList<String> ALS;
	public panGl_ALS2 pALS2;
	public String path;
	public DrdonAsLR() throws Exception{
		init();
		initmEnu();
	}
	protected void init() throws Exception{
		path="bd/DrdonAsLR.txt";
		ALS=tools.File_to_ALS(new File(path));
		setVisible(true);
		setTitle("fEnGtR(DrdonAsLR)");
		setExtendedState(MAXIMIZED_BOTH);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JSP=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSP.setDividerLocation(100);
		JSP.setDividerSize(0);
		pALS2=new panGl_ALS2(ALS);
		JSP.setBottomComponent(pALS2);
		setContentPane(JSP);
		rifrGq();
	}
	protected void initmEnu(){
		JMenuBar JMB=new JMenuBar();
		JMenu JM1=new JMenu("aksyM");
		{
			JMenuItem JMI=new JMenuItem("ajHte 1 taq");
			JMI.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						ajHte_1_taq();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1,InputEvent.CTRL_MASK));
			JM1.add(JMI);
			JMB.add(JM1);
		}
		{
			JMenuItem JMI=new JMenuItem("lAse fEnGtr(PrNsipal)");
			JMI.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						new prNsipal();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1,InputEvent.CTRL_MASK));
			JM1.add(JMI);
			JMB.add(JM1);
		}
		{
			JMenu JM2=new JMenu("Gd");
			JMB.add(JM2);
		}
		setJMenuBar(JMB);
	}
	@SuppressWarnings("static-access")
	public void ajHte_1_taq() throws IOException{
		JOptionPane jop=new JOptionPane();
		String s=jop.showInputDialog(null,"nHvGl taq","",JOptionPane.QUESTION_MESSAGE);
		if(s!=""&&s!=null){
			ALS.add(0,s);
		}
		sGyv();
		rifrGq();
	}
	public void sGyv() throws IOException{
		tools.ALS_to_file(ALS,path);
	}
	public void rifrGq() throws IOException{
		{
			if(ALS.size()==0){
				ALS.add("nE ryN fGr");
			}
			JPanel JP=new JPanel();
			JP.setLayout(new GridLayout(1,3));
			JLabel JL=new JLabel("a fGr :");
			JL.setFont(new Font("Cambria",Font.PLAIN,20));
			JP.add(JL);
			JLabel JL2=new JLabel(ALS.get(0));
			JL2.setFont(new Font("Cambria",Font.PLAIN,20));
			JP.add(JL2);
			{
				JPanel JP2=new JPanel();
				JP2.setLayout(new GridLayout(3,1));
				{
					JButton JB=new JButton("+");
					JB.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								ajHte_1_taq();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					});
					JP2.add(JB);
				}
				{
					JButton JB=new JButton("-");
					JB.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								ALS.remove(0);
								rifrGq();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					});
					JP2.add(JB);
				}
				{
					JButton JB=new JButton("�");
					JB.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								ALS.add(ALS.get(0));
								ALS.remove(0);
								rifrGq();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					});
					JP2.add(JB);
				}
				JP.add(JP2);
			}
			JSP.setTopComponent(JP);
		}
		pALS2.rifrGq();
		sGyv();
		revalidate();
	}
}

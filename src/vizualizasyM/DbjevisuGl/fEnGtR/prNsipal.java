package vizualizasyM.DbjevisuGl.fEnGtR;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import modGllojik.text_sequensor;
import modGllojik.tools;
import modGllojik.algoritm.jeneratLR.kMbinezM;
import vizualizasyM.ekritur.alfabe;
public class prNsipal extends JFrame{
	public static final long serialVersionUID=1L;
	public JPanel JP;
	public prNsipal() throws Exception{
		init();
		initbodi();
		initmEnu();
		revalidate();
	}
	public void init() throws Exception{
		JP=new JPanel();
		setVisible(true);
		setTitle("fEnGtr_prNsipal");
		setSize(1000,700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setContentPane(JP);
	}
	public void initbodi(){
		ArrayList<JButton> buttonListForGeneralWindow=new ArrayList<>();
		{
			JButton JB=new JButton("fEnGtR(tri)");
			JB.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						new tri();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			buttonListForGeneralWindow.add(JB);
		}
		{
			JButton JB=new JButton("fEnGtR(graf(Hti))");
			JB.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						new grafdEfMksyM();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			buttonListForGeneralWindow.add(JB);
		}
		{
			JButton JB=new JButton("fEnGtR(DrdonAsLR)");
			JB.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						new DrdonAsLR();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			buttonListForGeneralWindow.add(JB);
		}
		{
			JButton JB=new JButton("fEnGtR(lNgwistik)");
			JB.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						new lNgwistik();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			buttonListForGeneralWindow.add(JB);
		}
		{
			JButton JB=new JButton("fEnGtR(jGstyonGr aleatwar)");
			JB.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						new Random_manager();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			buttonListForGeneralWindow.add(JB);
		}
		
		for(JButton JB:buttonListForGeneralWindow){
			JP.setLayout(new GridLayout(buttonListForGeneralWindow.size(),1));
			JP.add(JB);
		}
	}
	public void initmEnu(){
		JMenuBar JMB=new JMenuBar();
		ajHt_JMenu(JMB);
		setJMenuBar(JMB);
	}
	public void ajHt_JMenu(JMenuBar JMB){
		{
			JMenu JM=new JMenu("aksyM");
			{
				JMenu JM2=new JMenu("fEnGtR");
				JM.add(JM2);
				{
					JMenuItem JMI=new JMenuItem("tri");
					JM2.add(JMI);
					JMI.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								new tri();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					});
					JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1,InputEvent.CTRL_MASK));
				}
				{
					JMenuItem JMI=new JMenuItem("plotdEfMksyM");
					JMI.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								new grafdEfMksyM();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					});
					JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD2,InputEvent.CTRL_MASK));
					JM2.add(JMI);
				}
			}
			{
				JMenu menu_jenerator=new JMenu("jeneratLR");
				{
					JMenuItem JMI=new JMenuItem("jenGr(kMbinezMrekursif)");
					JMI.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								alfabe a=new alfabe(tools.import_File());
								if(a!=null){
									a.kMsDn.add(0,"");
									a.vwayGl.add(0,"");
									ArrayList<ArrayList<String>> ALALS=new ArrayList<>();
									ALALS.add(a.kMsDn);
									ALALS.add(a.vwayGl);
									ALALS.add(a.kMsDn);
									ALALS.add(a.kMsDn);
									ALALS.add(a.vwayGl);
									ArrayList<String> ALS=kMbinezM.jenGrkMbinezMrekursif(ALALS);
									tools.ALS_to_file_manual(ALS);
								}
							}catch(Exception e){
								// okµ fiqye a ete qwazi
							}
						}
					});
					JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD3,InputEvent.CTRL_MASK));
					menu_jenerator.add(JMI);
				}
				{
					JMenuItem JMI=new JMenuItem("jenGr(kMbinezMiteratif)");
					JMI.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								tools.ALS_to_file_manual(kMbinezM.jenGrkMbinezMiteratif(fiqye_vGR_ALALS()));
							}catch(Exception e){
								// okµ fiqye a ete qwazi
							}
						}
					});
					JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD4,InputEvent.CTRL_MASK));
					menu_jenerator.add(JMI);
				}
				{
					JMenuItem JMI=new JMenuItem("jenGr(moaleatwar)");
					JMI.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								new lNgwistik();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					});
					JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD5,InputEvent.CTRL_MASK));
					menu_jenerator.add(JMI);
				}
				JM.add(menu_jenerator);
			}
			JMB.add(JM);
			JMenu menu_tools=new JMenu("Hti");
			JM.add(menu_tools);
			{
				{
					JMenuItem JMI=new JMenuItem("sekAsLR dE fiqye");
					JMI.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent event){
							try{
								text_sequensor.refactor();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					});
					JMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD6,InputEvent.CTRL_MASK));
					menu_tools.add(JMI);
				}
				JM.add(menu_tools);
			}
		}
		JMenu JM2=new JMenu("Gd");
		{
			JMenuItem JMI=new JMenuItem("lisAs");
			JMI.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					try{
						JFrame j=new JFrame();
						j.setVisible(true);
						j.setTitle("lisAs jeneral£");
						j.setSize(1200,1000);
						j.setLocationRelativeTo(null);
						j.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						JPanel JP=new JPanel();
						JTextArea JTA=new JTextArea();
						JTA.setLineWrap(true);
						JTA.setEditable(false);
						JTA.setSize(1200,800);
						JTA.setText(tools.File_tH_String(new File("LICENCE.TXT")));
						JP.setLayout(new BorderLayout());
						JP.add(new JScrollPane(JTA),BorderLayout.CENTER);
						j.setContentPane(JP);
						revalidate();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
			JM2.add(JMI);
		}
		JMB.add(JM2);
	}
	public ArrayList<ArrayList<String>> fiqye_vGR_ALALS() throws Exception,IOException{
		ArrayList<ArrayList<String>> ALALS=tools.cut_ALS_To_ALALS(tools.File_to_ALS(tools.import_File()));
		return ALALS;
	}
}

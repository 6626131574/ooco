package vizualizasyM.DbjevisuGl;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
public class PanelToSortText extends JPanel{
	public static final long serialVersionUID=1L;
	public FusionSortManager jtf;
	public JSplitPane JSP;
	public PanelToSortText(File F) throws Exception{
		jtf=new FusionSortManager(F);
		build();
	}
	public PanelToSortText(ArrayList<String> ALS) throws Exception{
		jtf=new FusionSortManager(ALS);
		build();
	}
	public void build(){
		removeAll();
		JSP=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSP.setResizeWeight(0.2);
		JSP.setDividerSize(4);
		add(JSP);
		JPanel JP=new JPanel();
		JP.setLayout(new GridLayout(2,1));
		ArrayList<Object> ALO=jtf.get_next_couple();
		JButton JB_1=new JButton((String)ALO.get(0));
		JButton JB_2=new JButton((String)ALO.get(1));
		JB_1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				jtf.choose(1);
				build();
				repaint();
				revalidate();
			}
		});
		JB_2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent event){
				jtf.choose(2);
				build();
				repaint();
				revalidate();
			}
		});
		JP.add(JB_1);
		JP.add(JB_2);
		JSP.setTopComponent(JP);
		JSplitPane JSP2=new JSplitPane(JSplitPane.VERTICAL_SPLIT);
		JSP2.setResizeWeight(0.2);
		JSP2.setDividerSize(4);
		JSP.setBottomComponent(JSP2);
		panGlALALS ptd1=new panGlALALS(jtf.coupleOfListForSort.get(1));
		JSP2.setTopComponent(ptd1);
		panGlALALS ptd2=new panGlALALS(jtf.coupleOfListForSort.get(2));
		JSP2.setBottomComponent(ptd2);
	}
}

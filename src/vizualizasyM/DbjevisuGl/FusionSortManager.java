package vizualizasyM.DbjevisuGl;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import modGllojik.tools;
public class FusionSortManager{
	public ArrayList<ArrayList<ArrayList<String>>> coupleOfListForSort;
	public int									   sayzDfseluldGstinasyM;
	public boolean								   finish;
	private void init(){
		finish=false;
		coupleOfListForSort=new ArrayList<>();
		coupleOfListForSort.add(new ArrayList<>());
		coupleOfListForSort.add(new ArrayList<>());
		ArrayList<ArrayList<String>> destinationList=new ArrayList<>();
		destinationList.add(new ArrayList<>());
		coupleOfListForSort.add(2,destinationList);
		sayzDfseluldGstinasyM=2;
	}
	public FusionSortManager(File F) throws IOException{
		init();
		coupleOfListForSort.set(1,tools.transform_ALS_to_ALALS(tools.File_to_ALS(F)));
	}
	public FusionSortManager(ArrayList<String> ALS) throws IOException{
		init();
		coupleOfListForSort.set(1,tools.transform_ALS_to_ALALS(ALS));
	}
	public void rifrGq_dGstinasyM(){
		ArrayList<ArrayList<String>> listdGstinasyM=new ArrayList<>();
		listdGstinasyM.add(new ArrayList<>());
		coupleOfListForSort.set(2,listdGstinasyM);
		sayzDfseluldGstinasyM*=2;
	}
	public void choose(int qwa){
		if(!finish){
			ArrayList<String> selulAkHR=coupleOfListForSort.get(2).get(coupleOfListForSort.get(2).size()-1);
			if(selulAkHR.size()>=sayzDfseluldGstinasyM){
				coupleOfListForSort.get(2).add(new ArrayList<>());
				selulAkHR=coupleOfListForSort.get(2).get(coupleOfListForSort.get(2).size()-1);
			}
			if(qwa==1){
				selulAkHR.add(coupleOfListForSort.get(1).get(0).get(0));
				coupleOfListForSort.get(1).get(0).remove(0);
			}else{
				selulAkHR.add(coupleOfListForSort.get(1).get(1).get(0));
				coupleOfListForSort.get(1).get(1).remove(0);
			}
			if(coupleOfListForSort.get(1).get(0).size()==0){
				for(String S:coupleOfListForSort.get(1).get(1)){
					selulAkHR.add(S);
				}
				coupleOfListForSort.get(1).remove(0);
				coupleOfListForSort.get(1).remove(0);
			}else if(coupleOfListForSort.get(1).get(1).size()==0){
				for(String S:coupleOfListForSort.get(1).get(0)){
					selulAkHR.add(S);
				}
				coupleOfListForSort.get(1).remove(0);
				coupleOfListForSort.get(1).remove(0);
			}
			switch(coupleOfListForSort.get(1).size()){
				case 0:
					coupleOfListForSort.set(1,coupleOfListForSort.get(2));
					rifrGq_dGstinasyM();
					break;
				case 1:
					coupleOfListForSort.get(2).add(coupleOfListForSort.get(1).get(0));
					coupleOfListForSort.set(1,coupleOfListForSort.get(2));
					rifrGq_dGstinasyM();
					break;
			}
		}
		if(coupleOfListForSort.get(1).size()<2){
			finish=true;
		}
	}
	public ArrayList<Object> get_next_couple(){
		ArrayList<Object> ALO2=new ArrayList<>();
		if(!finish){
			ALO2.add(coupleOfListForSort.get(1).get(0).get(0));
			ALO2.add(coupleOfListForSort.get(1).get(1).get(0));
		}else{
			ALO2.add("fini");
			ALO2.add("fini");
		}
		return ALO2;
	}
}
package modGllojik;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFileChooser;
public abstract class tools{// Hti static
	public static ArrayList<String> File_to_ALS(File F) throws IOException{
		if(F!=null){
			ArrayList<String> ALS=new ArrayList<>();
			InputStream IS=new FileInputStream(F);
			InputStreamReader ISR=new InputStreamReader(IS);
			BufferedReader BR=new BufferedReader(ISR);
			String S;
			while((S=BR.readLine())!=null){
				ALS.add(S);
			}
			BR.close();
			return ALS;
		}else{
			return null;
		}
	}
	public static String File_tH_String(File F) throws IOException{
		if(F!=null){
			String S="";
			InputStream IS=new FileInputStream(F);
			InputStreamReader ISR=new InputStreamReader(IS);
			BufferedReader BR=new BufferedReader(ISR);
			String S2;
			while((S2=BR.readLine())!=null){
				S+=S2+"\n";
			}
			BR.close();
			return S;
		}else{
			return null;
		}
	}
	public static ArrayList<ArrayList<String>> transform_ALS_to_ALALS(ArrayList<String> ALS){
		ArrayList<ArrayList<String>> ALALS=new ArrayList<>();
		for(String S:ALS){
			ArrayList<String> ALS2=new ArrayList<>();
			ALS2.add(S);
			ALALS.add(ALS2);
		}
		return ALALS;
	}
	public static ArrayList<ArrayList<String>> cut_ALS_To_ALALS(ArrayList<String> ALS){
		ArrayList<ArrayList<String>> ALALS=new ArrayList<>();
		for(String S:ALS){
			ArrayList<String> ALS2=new ArrayList<>();
			int l=S.length();
			for(int i=0;i<l;i++){
				ALS2.add(S.substring(i,i+1));
			}
			ALALS.add(ALS2);
		}
		return ALALS;
	}
	public static File import_File() throws Exception{
		JFileChooser JFC=new JFileChooser(new File("tmp/"));
		if(JFC.showOpenDialog(null)==JFileChooser.APPROVE_OPTION){
			return JFC.getSelectedFile();
		}
		return null;
	}
	public static void ALS_to_file_manual(ArrayList<String> ALS) throws IOException{
		JFileChooser JFC=new JFileChooser(new File("tmp/"));
		JFC.setDialogTitle("ArEjistRe la list aktuGl");
		if(JFC.showSaveDialog(null)==JFileChooser.APPROVE_OPTION){
			ALS_to_file(ALS,JFC.getSelectedFile().getAbsolutePath());
		}
	}
	public static void ALS_to_file(ArrayList<String> ALS,String path) throws IOException{
		File F=new File(path);
		PrintWriter PW=new PrintWriter(new BufferedWriter(new FileWriter(F)));
		for(String S:ALS){
			PW.println(S);
		}
		PW.close();
	}
	public static ArrayList<Character> ALC_dE_String(String S){
		ArrayList<Character> rezult=new ArrayList<>();
		int l=S.length();
		for(int i=0;i<l;i++){
			rezult.add(S.charAt(i));
		}
		return rezult;
	}
	public static String String_qtace_to_String_FR(String S){
		ArrayList<Character> inpHt=tools.ALC_dE_String(S);
		String rezult="";
		int l=inpHt.size();
		for(Character C:inpHt){
			switch(C){
			case '$':
				rezult+="gn";
				break;
			case 'R':
				rezult+="r";
				break;
			case 'r':
				rezult+="r";
				break;
			case '&':
				rezult+="r";
				break;
			case 'c':
				rezult+="l";
				break;
			case '€':
				rezult+="sh";
				break;
			case 'S':
				rezult+="th";
				break;
			case 'q':
				rezult+="ch";
				break;
			case 'o':
				rezult+="au";
				break;
			case 'D':
				rezult+="o";
				break;
			case 'M':
				rezult+="on";
				break;
			case 'E':
				rezult+="e";
				break;
			case 'e':
				rezult+="é";
				break;
			case 'è':
				rezult+="G";
				break;
			case 'L':
				rezult+="eu";
				break;
			case 'u':
				rezult+="u";
				break;
			case 'U':
				rezult+="u";
				break;
			case 'N':
				rezult+="in";
				break;
			case 'A':
				rezult+="en";
				break;
			case 'µ':
				rezult+="un";
				break;
			case 'é':
				rezult+="e";
				break;
			default:
				rezult+=""+C;
			}
		}
		for(int i=0;i<l;i++){}
		return rezult;
	}
	public static ArrayList<Object> mix_ALO(ArrayList<Object> ALO){
		Random R=new Random();
		for(int i=0;i<ALO.size();i++){
			Object memwar=ALO.get(i);
			int alea=R.nextInt(ALO.size());
			ALO.set(i,ALO.get(alea));
			ALO.set(alea,memwar);
		}
		return ALO;
	}
}

package modGllojik.algoritm;
import java.util.ArrayList;
public abstract class aritmetik{
	public static ArrayList<Integer[]> pgcd(int a,int b){
		ArrayList<Integer[]> AL=new ArrayList<Integer[]>();
		// init
		if(a>b){
			Integer[] v0={1,0,a};
			AL.add(v0);
			Integer[] v1={0,1,b};
			AL.add(v1);
		}else{
			Integer[] v0={1,0,b};
			AL.add(v0);
			Integer[] v1={0,1,a};
			AL.add(v1);
		}
		// kalkul
		while(AL.get(AL.size()-1)[2]>0){
			Integer[] vn_1=AL.get(AL.size()-1);
			Integer[] vn_2=AL.get(AL.size()-2);
			Integer q=vn_2[2]/vn_1[2];
			Integer[] vn={-q*vn_1[0]+vn_2[0],-q*vn_1[1]+vn_2[1],-q*vn_1[2]+vn_2[2]};
			AL.add(vn);
		}
		return AL;
	}
	public static void printALdu_pgcd(ArrayList<Integer[]> AL){
		for(Integer[] vGktLR:AL){
			System.out.println("("+vGktLR[0]+","+vGktLR[1]+","+vGktLR[2]+")");
		}
		System.out.println("le pgcd est:"+AL.get(AL.size()-2)[2]);
	}
}

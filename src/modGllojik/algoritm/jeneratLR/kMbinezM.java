package modGllojik.algoritm.jeneratLR;
import java.util.ArrayList;
public abstract class kMbinezM{
	public static ArrayList<String> jenGrkMbinezMrekursif(ArrayList<ArrayList<String>> ALALS){
		ArrayList<String> ALS=new ArrayList<>();
		String S="";
		apGlrekursif(S,ALALS,0,ALS);
		return ALS;
	}
	private static void apGlrekursif(String S,ArrayList<ArrayList<String>> ALALS,int etaj,ArrayList<String> rezult){
		int l=ALALS.get(etaj).size();
		if(etaj<ALALS.size()-1){
			for(int i=0;i<l;i++){
				String S2=S+ALALS.get(etaj).get(i);
				apGlrekursif(S2,ALALS,etaj+1,rezult);
			}
		}else{
			for(int i=0;i<l;i++){
				String S3=S+ALALS.get(etaj).get(i);
				rezult.add(S3);
			}
		}
	}
	public static ArrayList<String> jenGrkMbinezMiteratif(ArrayList<ArrayList<String>> ALALS){
		//init
		int n=ALALS.size();
		int bDrn[]=new int[n];
		for(int i=0;i<n;i++){
			bDrn[i]=ALALS.get(i).size()-1;
		}
		int kMtLR[]=new int[n];
		ArrayList<String> ALS=new ArrayList<>();
		//kalkul
		while(true){
			if(kMtLR[0]>bDrn[0]){
				break;
			}
			//kreasyM dE la kMbinezM
			String S="";
			for(int i=0;i<n;i++){
				S+=ALALS.get(i).get(kMtLR[i]);
			}
			ALS.add(S);
			//NkremAtasyM du kMtLR
			kMtLR[n-1]++;
			for(int i=n-1;0<i;i--){
				if(kMtLR[i]>bDrn[i]){
					kMtLR[i]=0;
					kMtLR[i-1]++;
				}else{
					break;
				}
			}
		}
		return ALS;
	}
}

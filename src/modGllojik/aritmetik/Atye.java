package modGllojik.aritmetik;
import java.io.File;
import vizualizasyM.ekritur.alfabe;
public class Atye extends asGrsyM{
	public alfabe e;
	public Atye(String s,File F) throws Exception{
		super(s);
		e=new alfabe(F);
	}
	public void adisyM(Atye A){
		String rGz="";
		String rGst="b";
		int l=max(O_s.length(),A.O_s.length());
		String k1;
		String k2;
		for(int i=0;i<l;i++){
			if(i%2==0){
				k1=this.O_s.substring(i,i+1);
				k2=A.O_s.substring(i,i+1);
				String tab5[]=M_1d9s92n_kos2n(k1,rGst);
				String tab1[]=M_1d9s92n_kos2n(tab5[0],k2);
				rGz+=tab1[0];
				String tab6[]=M_1d9s92n_vw1y5l(tab5[1],tab1[1]);
				rGst=tab6[0];
			}else{
				k1=this.O_s.substring(i,i+1);
				k2=A.O_s.substring(i,i+1);
				String tab5[]=M_1d9s92n_vw1y5l(k1,rGst);
				String tab1[]=M_1d9s92n_vw1y5l(tab5[0],k2);
				rGz+=tab1[0];
				String tab6[]=M_1d9s92n_kos2n(tab5[1],tab1[1]);
				rGst=tab6[0];
			}
		}
		rGz+=rGst;
		if(rGz.length()%2!=0){
			rGz+=0;
		}
		this.O_s=rGz;
	}
	public String[] M_1d9s92n_vw1y5l(String S,String S2){
		int i=(e.vwayGl.indexOf(S)+e.vwayGl.indexOf(S2))%e.vwayGl.size();
		int j=(e.vwayGl.indexOf(S)+e.vwayGl.indexOf(S2))/e.vwayGl.size();
		S=(String)e.vwayGl.get(i);
		S2=(String)e.kMsDn.get(j);
		String tab[]={S,S2};// {r5z;r5st}
		return tab;
	}
	public String[] M_1d9s92n_kos2n(String S,String S2){
		int i=(e.kMsDn.indexOf(S)+e.kMsDn.indexOf(S2))%e.kMsDn.size();
		int j=(e.kMsDn.indexOf(S)+e.kMsDn.indexOf(S2))/e.kMsDn.size();
		S=(String)e.kMsDn.get(i);
		S2=(String)e.vwayGl.get(j);
		String t1b[]={S,S2};// {r5z;r5st}
		return t1b;
	}
	public int max(int a,int b){
		if(a>b){ return a; }
		return b;
	}
	public int min(int a,int b){
		return -max(-a,-b);
	}
	public String O_s;
}

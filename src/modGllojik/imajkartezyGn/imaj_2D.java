package modGllojik.imajkartezyGn;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import diksyonGr.evaluasyM;
public class imaj_2D extends JPanel{
	private static final long serialVersionUID=-7924599028622650497L;
	BufferedImage			  p;
	Graphics				  g;
	public imaj_2D(evaluasyM f,Double p1,Double d1,Double k1,Double m,Integer v,Integer b1){
		// foksyo,1bs9s(if),1bs9s(s7p),2rd0n4(if),v1l5e(s7p),l1rj5r,0t5r
		// 9m1j;1v5k;trasp1ras
		p=new BufferedImage(v,b1,BufferedImage.TYPE_INT_ARGB);
		g=p.getGraphics();
		// d4s9nl4zif2rm1syod7kot5kst
		// k1dr
		g.setColor(Color.getHSBColor((float)0,(float)0,(float)0));
		g.drawLine(0,0,0,b1-1);
		g.drawLine(0,0,v-1,0);
		g.drawLine(0,b1-1,v-1,b1-1);
		g.drawLine(v-1,0,v-1,b1-1);
		g.drawLine(v/2,0,v/2,b1-1);
		// 1ks
		g.drawLine(0,b1/2,v-1,b1/2);
		g.drawLine(0,b1/2,v-1,b1/2);
		// l9m9t1syo
		// X
		g.setColor(Color.getHSBColor((float)0,(float)0,(float)0.5));
		Integer i=0;// ikr4ma;stad1e
		Double p=0.8;// coef;d3;f3n5tr;k1rt4zyi
		Integer t=(int)(v*(1-p));// 1bs9s(g2q)d4si
		Integer k=(int)(v*p);// 1bs9s(drw1t)d4si
		for(i=v%40/2;i<v;i+=40){
			g.drawLine(i+10,t,i+30,t);
			g.drawLine(i+10,k,i+30,k);
		}
		// Y
		Integer g=(int)(b1*(1-p));// 2rd0n4(b1s)d4si
		Integer l=(int)(b1*p);// 1bs9s(0)d4si
		// tr1s1j;a;pwit9y4
		for(i=b1%40/2;i<b1;i+=40){
			this.g.drawLine(g,i+10,g,i+30);
			this.g.drawLine(l,i+10,l,i+30);
		}
		// f3n5tr5kst5ry6e
		// d4s9n(pwid3l1foksyo)
		Double n=(double)0;// 1bs9s
		Integer j=0;
		Double n1[]=new Double[v];// t1bl(v1l5r(foksyo))
		for(i=0;i<v;i++){// 9n9t(t1bl)
			n1[i]=new Double(0);
		}
		// k1l7l(v1l5r(foksyo))
		// -∞
		// -1.E50..p1
		for(i=1;i<t;i++){
			n=Math.tan((i.doubleValue()-t)/(v-k)*Math.PI/2)+p1;
			n1[i]=f.kalkulnumerik(n);
		}
		// tr1s1j(gr1f;k1rt4zyi)atr;p1..d1
		Double l1=(d1.doubleValue()-p1)/(k-t);// k05f;l9n45e
		for(i=t;i<k;i++){
			n=(i-t)*l1+p1;
			n1[i]=f.kalkulnumerik(n);
		}
		// d1..1.E50
		for(i=k;i<v;i++){
			n=Math.tan((i.doubleValue()-k)/(v-k)*Math.PI/2)+d1;
			n1[i]=f.kalkulnumerik(n);
		}
		// ∞
		// d4si
		l1=(double)(l-g)/(m-k1);
		for(i=1;i<v;i++){
			// [-∞[
			// ]-1E50..d1[
			
			// [p1..d1[
			
			if(k1<n1[i]&&n1[i]<m){
				j=(int)((n1[i]-k1)*l1+g);
				this.p.setRGB(i-1,j-1,0xFFFF0000);
				this.p.setRGB(i-1,j,0xFFFF0000);
				this.p.setRGB(i-1,j+1,0xFFFF0000);
				this.p.setRGB(i,j-1,0xFFFF0000);
				this.p.setRGB(i,j,0xFFFF0000);
				this.p.setRGB(i,j+1,0xFFFF0000);
				this.p.setRGB(i+1,j-1,0xFFFF0000);
				this.p.setRGB(i+1,j,0xFFFF0000);
				this.p.setRGB(i+1,j+1,0xFFFF0000);
			}
		}
		// ]d1..1E50[
		// [∞]
	}
	public void paintComponent(Graphics g){
		g.drawImage(p,80,80,this);
	}
}

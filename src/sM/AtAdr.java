package sM;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
/** Sample audio recorder */
public class AtAdr extends Thread{
	/** The TargetDataLine that we'll use to read data from */
	public final TargetDataLine		  line;
	/** The audio format type that we'll encode the audio data with */
	public final AudioFileFormat.Type targetType=AudioFileFormat.Type.WAVE;
	/** The AudioInputStream that we'll read the audio data from */
	public final AudioInputStream	  inputStream;
	/** The file that we're going to write data out to */
	public File						  file;
	/**
	 * Creates a new Audio Recorder 
	 */
	public AtAdr(String a) throws LineUnavailableException{
		// Create an AudioFormat that specifies how the recording will be performed
		// In this example we'll 44.1Khz, 16-bit, stereo
		// (Encoding technique, Sample Rate, bits in each channel, channels (2=stereo), bytes in
		// each frame, frames per second, Big-endian (true) or little-endian (false))
		AudioFormat audioFormat=new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,44100.0F,16,2,4,44100.0F,false);
		// Create our TargetDataLine that will be used to read audio data by first
		// creating a DataLine instance for our audio format type
		DataLine.Info info=new DataLine.Info(TargetDataLine.class,audioFormat);
		// Next we ask the AudioSystem to retrieve a line that matches the DataLine Info
		line=(TargetDataLine)AudioSystem.getLine(info);
		// Open the TargetDataLine with the specified format
		line.open(audioFormat);
		// Create an AudioInputStream that we can use to read from the line
		inputStream=new AudioInputStream(line);
		// Create the output file
		a="tmp/"+a;
		file=new File(a);
	}
	public void startRecording(){
		// Start the TargetDataLine
		line.start();
		// Start our thread
		this.start();
	}
	public void stopRecording(){
		// Stop and close the TargetDataLine
		line.stop();
		line.close();
	}
	public void run(){
		// Ask the AudioSystem class to write audio data from the audio input stream
		// to our file in the specified data type (PCM 44.1Khz, 16-bit, stereo)
		try{
			AudioSystem.write(inputStream,targetType,file);
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	public String getPath(){
		return file.getAbsolutePath();
	}
	public void M_artjvs3r() throws InterruptedException{
		System.out.println("Usage: Hti Recorder <filename>\n"+"Default file will be used\n");
		// Start the recorder
		System.out.println("d�marage de l'�coute");
		this.startRecording();
		// Stop the recorder after 3 sec
		Thread.sleep(3000);
		this.stopRecording();
		System.out.println("Recording complete : "+this.getPath());
	}
	
}